# Vietnamese translation for Contact Lookup Applet.
# Copyright © 2007 Gnome i18n Project for Vietnamese.
# Clytie Siddall <clytie@riverland.net.au>, 2005-2007.
#
msgid ""
msgstr ""
"Project-Id-Version: contact-lookup-applet GNOME HEAD\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2007-07-04 03:34+0100\n"
"PO-Revision-Date: 2007-09-14 23:24+0930\n"
"Last-Translator: Clytie Siddall <clytie@riverland.net.au>\n"
"Language-Team: Vietnamese <gnomevi-list@lists.sourceforge.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: LocFactoryEditor 1.7b1\n"

#: ../data/GNOME_ContactLookupApplet.server.in.in.h:1
msgid "Address Book Search"
msgstr "Tìm qua Sổ địa chỉ"

#: ../data/GNOME_ContactLookupApplet.server.in.in.h:2
msgid "Search for a person in your address book"
msgstr "Tìm kiếm một người nào đó trong sổ địa chỉ của bạn."

#: ../data/GNOME_ContactLookupApplet.xml.h:4
#.
#. <menuitem name="help" verb="help" _label="_Help" pixtype="stock" pixname="gtk-help"/>
#.
msgid "_About"
msgstr "_Giới thiệu"

#: ../data/contact-lookup-applet.glade.h:1
msgid "<b>\"Foo\" is online</b>"
msgstr "<b>« Phu » hiện thời trực tuyến.</b>"

#: ../data/contact-lookup-applet.glade.h:2
msgid "<b>Electronic Mail</b>"
msgstr "<b>Thư điện tử</b>"

#: ../data/contact-lookup-applet.glade.h:3
msgid "<b>Instant Messaging</b>"
msgstr "<b>Tin nhắn</b>"

#: ../data/contact-lookup-applet.glade.h:4
msgid "<b>Video Conferencing</b>"
msgstr "<b>Hội thảo ảnh động</b>"

#: ../data/contact-lookup-applet.glade.h:5
msgid "ADDRESS"
msgstr "ĐỊA CHỈ"

#: ../data/contact-lookup-applet.glade.h:6
msgid "Address Card"
msgstr "Thẻ địa chỉ"

#: ../data/contact-lookup-applet.glade.h:7
msgid "Compose _Mail Message\t"
msgstr "Viết _thư\t"

#: ../data/contact-lookup-applet.glade.h:8
msgid "HOME PAGE"
msgstr "TRANG CHỦ"

#: ../data/contact-lookup-applet.glade.h:9
msgid "NAME"
msgstr "TÊN"

#: ../data/contact-lookup-applet.glade.h:10
msgid "PHONE"
msgstr "ĐIỆN THOẠI"

#: ../data/contact-lookup-applet.glade.h:11
msgid "Send _Instant Message"
msgstr "Gửi t_in nhắn"

#: ../data/contact-lookup-applet.glade.h:12
msgid "Start _Video Conference"
msgstr "K_hởi tạo hội thảo ảnh động"

#: ../data/contact-lookup-applet.glade.h:13
msgid "VIDEO"
msgstr "ẢNH ĐỘNG"

#: ../data/contact-lookup-applet.glade.h:14
msgid "_Edit Contact Information"
msgstr "_Sửa thông tin liên lạc"

#: ../data/contact-lookup-applet.glade.h:15
msgid "_Go"
msgstr "_Tới"

#: ../src/contact-dialog.c:58
#, c-format
msgid ""
"<span weight=\"bold\" size=\"large\">Cannot show URL %s:</span>\n"
"\n"
"%s"
msgstr ""
"<span weight=\"bold\" size=\"large\">Không thể hiển thị địa chỉ URL %s:</"
"span>\n"
"\n"
"%s"

#: ../src/contact-dialog.c:128
#, c-format
msgid ""
"<span weight=\"bold\" size=\"large\">Cannot send email to %s:</span>\n"
"\n"
"%s"
msgstr ""
"<span weight=\"bold\" size=\"large\">Không thể gửi thư cho %s:</span>\n"
"\n"
"%s"

#: ../src/contact-dialog.c:147
#, c-format
msgid ""
"<span weight=\"bold\" size=\"large\">Cannot start conference with %s:</"
"span>\n"
"\n"
"%s"
msgstr ""
"<span weight=\"bold\" size=\"large\">Không thể bắt đầu hội thảo với %s:</"
"span>\n"
"\n"
"%s"

#: ../src/contact-dialog.c:419
#: ../src/contact-dialog.c:425
msgid " (home)\n"
msgstr " (ở nhà)\n"

#: ../src/contact-dialog.c:431
#: ../src/contact-dialog.c:437
msgid " (work)\n"
msgstr " (chỗ làm)\n"

#: ../src/contact-dialog.c:443
msgid " (mobile)\n"
msgstr " (di động)\n"

#: ../src/contact-lookup-applet.c:59
#, c-format
msgid "Unable to search your address book: %s"
msgstr "Không thể tìm kiếm qua sổ địa chỉ của bạn: %s"

#: ../src/contact-lookup-applet.c:63
msgid "Search your address book"
msgstr "Tìm qua sổ địa chỉ của bạn"

#: ../src/contact-lookup-applet.c:111
#, c-format
msgid ""
"<span size='larger' weight='bold'>Error accessing addressbook</span>\n"
"\n"
"%s"
msgstr ""
"<span size='larger' weight='bold'>Gặp lỗi khi truy cập sổ địa chỉ.</span>\n"
"\n"
"%s"

#: ../src/contact-lookup-applet.c:142
msgid "An applet to search your address book."
msgstr "Tiểu dụng tìm qua sổ địa chỉ của bạn."

#: ../src/contact-lookup-applet.c:154
msgid ""
"<span size='larger' weight='bold'>No Address Books Available</span>\n"
"\n"
"There are no address books selected for auto-completion. To select which "
"address books to use for completion, go to Edit -> Preferences -> "
"Autocompletion in Evolution."
msgstr ""
"<span size='larger' weight='bold'>Không có sổ địa chỉ sẵn sàng.</span>\n"
"\n"
"Không có sổ địa chỉ được chọn để tự động gõ xong. Để chọn những sổ "
"địa chỉ cần dùng khi gõ xong, chọn mục trình đơn « Sửa > Tùy thích > Tự động gõ xong » trong trình Evolution."

#: ../src/e-contact-entry.c:153
#, c-format
msgid "Cannot get contact: %s"
msgstr "Không thể lấy liên lạc: %s"

#: ../src/e-contact-entry.c:185
#, c-format
msgid "Could not find contact: %s"
msgstr "Không tìm thấy liên lạc: %s"

#: ../src/e-contact-entry.c:383
msgid "Cannot create searchable view."
msgstr "Không thể tạo ô xem trong đó có thể tìm kiếm."

#: ../src/e-contact-entry.c:836
msgid "Success"
msgstr "Thành công"

#: ../src/e-contact-entry.c:838
msgid "An argument was invalid."
msgstr "Gặp một đối số không hợp lệ."

#: ../src/e-contact-entry.c:840
msgid "The address book is busy."
msgstr "Sổ địa chỉ đang bận."

#: ../src/e-contact-entry.c:842
msgid "The address book is offline."
msgstr "Sổ địa chỉ hiện thời ngoại tuyến."

#: ../src/e-contact-entry.c:844
msgid "The address book does not exist."
msgstr "Sổ địa chỉ không tồn tại."

#: ../src/e-contact-entry.c:846
msgid "The \"Me\" contact does not exist."
msgstr "Không có liên lạc được gọi là « Tôi »."

#: ../src/e-contact-entry.c:848
msgid "The address book is not loaded."
msgstr "Sổ địa chỉ chưa được nạp."

#: ../src/e-contact-entry.c:850
msgid "The address book is already loaded."
msgstr "Sổ địa chỉ đã được nạp."

#: ../src/e-contact-entry.c:852
msgid "Permission was denied when accessing the address book."
msgstr "Không có đủ quyền để truy cập sổ địa chỉ."

#: ../src/e-contact-entry.c:854
msgid "The contact was not found."
msgstr "Không tìm thấy liên lạc."

#: ../src/e-contact-entry.c:856
msgid "This contact ID already exists."
msgstr "ID liên lạc này đã tồn tại."

#: ../src/e-contact-entry.c:858
msgid "The protocol is not supported."
msgstr "Giao thức không được hỗ trợ."

#: ../src/e-contact-entry.c:860
msgid "The operation was cancelled."
msgstr "Thao tác bị thôi."

#: ../src/e-contact-entry.c:862
msgid "The operation could not be cancelled."
msgstr "Không thể thôi thao tác này."

#: ../src/e-contact-entry.c:864
msgid "The address book authentication failed."
msgstr "Lỗi xác thực với sổ địa chỉ."

#: ../src/e-contact-entry.c:866
msgid ""
"Authentication is required to access the address book and was not given."
msgstr "Cần thiết xác thực để truy cập sổ địa chỉ mà chưa làm."

#: ../src/e-contact-entry.c:868
msgid "A secure connection is not available."
msgstr "Không có kết nối bảo mật sẵn sàng."

#: ../src/e-contact-entry.c:870
msgid "A CORBA error occurred whilst accessing the address book."
msgstr "Gặp lỗi CORBA khi truy cập sổ địa chỉ."

#: ../src/e-contact-entry.c:872
msgid "The address book source does not exist."
msgstr "Nguồn sổ địa chỉ không tồn tại."

#: ../src/e-contact-entry.c:874
#: ../src/e-contact-entry.c:877
msgid "An unknown error occurred."
msgstr "Gặp lỗi không rõ."

#: ../src/glade-utils.c:49
#, c-format
msgid "Couldn't find necessary glade file '%s'"
msgstr "Không tìm thấy tập tin glade cần thiết « %s »."

#: ../src/glade-utils.c:59
#, c-format
msgid "Glade file '%s' is missing widget '%s'."
msgstr "Tập tin glade « %s » thiếu ô điều khiển « %s »."

#: ../src/glade-utils.c:139
#, c-format
msgid "Glade file is missing widget '%s'"
msgstr "Tập tin glade thiếu ô điều khiển « %s »."

#~ msgid "An unknown error occured."
#~ msgstr "Gặp lỗi không xác định."
