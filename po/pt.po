# contact-lookup-applet Portuguese Translation.
# Copyright © 2006 contact-lookup-applet
# This file is distributed under the same license as the contact-lookup-applet package.
# <gass@otiliamatos.ath.cx>, 2006.
# 
msgid ""
msgstr ""
"Project-Id-Version: contact-lookup-applet\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2006-07-12 10:21+0200\n"
"PO-Revision-Date: 2006-07-13 23:19+0100\n"
"Last-Translator: Luis Matos <gass@otiliamatos.ath.cx>\n"
"Language-Team: Portuguese <gnome_pt@yahoogoups.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit"

#: ../data/GNOME_ContactLookupApplet.server.in.in.h:1
msgid "Address Book Search"
msgstr "Procura na Lista de Endereços"

#: ../data/GNOME_ContactLookupApplet.server.in.in.h:2
msgid "Search for a person in your address book"
msgstr "Procurar por uma pessoa na sua lista de endereços"

#.
#. <menuitem name="help" verb="help" _label="_Help" pixtype="stock" pixname="gtk-help"/>
#.
#: ../data/GNOME_ContactLookupApplet.xml.h:4
msgid "_About..."
msgstr "_Sobre..."

#: ../data/contact-lookup-applet.glade.h:1
msgid "<b>\"Foo\" is online</b>"
msgstr "<b>\"Foo\" está ligado</b>"

#: ../data/contact-lookup-applet.glade.h:2
msgid "<b>Electronic Mail</b>"
msgstr "<b>Correio Electrónico</b>"

#: ../data/contact-lookup-applet.glade.h:3
msgid "<b>Instant Messaging</b>"
msgstr "<b>Mensagens Instantâneas</b>"

#: ../data/contact-lookup-applet.glade.h:4
msgid "<b>Video Conferencing</b>"
msgstr "<b>Vídeo Conferência</b>"

#: ../data/contact-lookup-applet.glade.h:5
msgid "ADDRESS"
msgstr "ENDEREÇO"

#: ../data/contact-lookup-applet.glade.h:6
msgid "Address Card"
msgstr "Cartão de Endereço"

#: ../data/contact-lookup-applet.glade.h:7
msgid "Compose _Mail Message\t"
msgstr "Co_mpor Mensagem de Correio\t"

#: ../data/contact-lookup-applet.glade.h:8
msgid "HOME PAGE"
msgstr "PÁGINA INICIAL"

#: ../data/contact-lookup-applet.glade.h:9
msgid "NAME"
msgstr "NOME"

#: ../data/contact-lookup-applet.glade.h:10
msgid "PHONE"
msgstr "TELEFONE"

#: ../data/contact-lookup-applet.glade.h:11
msgid "Send _Instant Message"
msgstr "Enviar Mensagem _Instantânea"

#: ../data/contact-lookup-applet.glade.h:12
msgid "Start _Video Conference"
msgstr "Iniciar _Vídeo Conferência"

#: ../data/contact-lookup-applet.glade.h:13
msgid "VIDEO"
msgstr "VÍDEO"

#: ../data/contact-lookup-applet.glade.h:14
msgid "_Edit Contact Information"
msgstr "_Editar Informações do Contacto"

#: ../data/contact-lookup-applet.glade.h:15
msgid "_Go"
msgstr "_Ir Para"

#: ../src/contact-dialog.c:53
#, c-format
msgid ""
"<span weight=\"bold\" size=\"large\">Cannot show URL %s:</span>\n"
"\n"
"%s"
msgstr ""
"<span·weight=\"bold\"·size=\"large\">Impossível aceder ao endereço·%s:</span>\n"
"\n"
"%s"

#: ../src/contact-dialog.c:81
#, c-format
msgid ""
"<span weight=\"bold\" size=\"large\">Cannot send email to %s:</span>\n"
"\n"
"%s"
msgstr ""
"<span·weight=\"bold\"·size=\"large\">Incapaz de enviar email para %s:</span>\n"
"\n"
"%s"

#: ../src/contact-dialog.c:100
#, c-format
msgid ""
"<span weight=\"bold\" size=\"large\">Cannot start conference with %s:</"
"span>\n"
"\n"
"%s"
msgstr ""
"<span·weight=\"bold\"·size=\"large\">Incapaz de iniciar conferência com %s:</"
"span>\n"
"\n"
"%s"

#: ../src/contact-dialog.c:321
msgid " (home)\n"
msgstr " (casa)\n"

#: ../src/contact-dialog.c:327
msgid " (work)\n"
msgstr " (profissional)\n"

#: ../src/contact-dialog.c:333
msgid " (mobile)\n"
msgstr " (móvel)\n"

#: ../src/contact-lookup-applet.c:59
#, c-format
msgid "Unable to search your address book: %s"
msgstr "Incapaz de procurar na sua lista de endereços: %s"

#: ../src/contact-lookup-applet.c:63
msgid "Search your address book"
msgstr "Procurar na sua lista de endereços"

#: ../src/contact-lookup-applet.c:111
#, c-format
msgid ""
"<span size='larger' weight='bold'>Error accessing addressbook</span>\n"
"\n"
"%s"
msgstr ""
"<span size='larger' weight='bold'>Erro ao aceder à lista de endereços</span>\n"
"\n"
"%s"

#: ../src/contact-lookup-applet.c:142
msgid "An applet to search your address book."
msgstr "Uma applet para procurar na sua lista de endereços."

#: ../src/contact-lookup-applet.c:154
msgid ""
"<span size='larger' weight='bold'>No Address Books Available</span>\n"
"\n"
"There are no address books selected for auto-completion. To select which "
"address books to use for completion, go to Edit -> Preferences -> "
"Autocompletion in Evolution."
msgstr ""
"<span size='larger' weight='bold'>Nenhuma lista de endereços disponível</span>\n"
"\n"
"Não existem listas de endereços seleccionadas para auto-completar. Para seleccionar qual"
"a lista de endereços onde utilizar o auto-completar, ir a Editar -> Preferências ->"
"Autocompletar no Evolution."

#: ../src/e-contact-entry.c:153
#, c-format
msgid "Cannot get contact: %s"
msgstr "Incapaz de obter o contacto: %s"

#: ../src/e-contact-entry.c:185
#, c-format
msgid "Could not find contact: %s"
msgstr "Incapaz de encontrar o contacto: %s"

#: ../src/e-contact-entry.c:373
msgid "Cannot create searchable view."
msgstr "Incapaz de criar uma vista procurável."

#: ../src/e-contact-entry.c:826
msgid "Success"
msgstr "Sucesso"

#: ../src/e-contact-entry.c:828
msgid "An argument was invalid."
msgstr "Um argumento é inválido."

#: ../src/e-contact-entry.c:830
msgid "The address book is busy."
msgstr "A lista de endereços está ocupada."

#: ../src/e-contact-entry.c:832
msgid "The address book is offline."
msgstr "A lista de endereços está offline."

#: ../src/e-contact-entry.c:834
msgid "The address book does not exist."
msgstr "A lista de endereços não existe."

#: ../src/e-contact-entry.c:836
msgid "The \"Me\" contact does not exist."
msgstr "O contacto \"Me\" não existe."

#: ../src/e-contact-entry.c:838
msgid "The address book is not loaded."
msgstr "A lista de endereços não está carregada."

#: ../src/e-contact-entry.c:840
msgid "The address book is already loaded."
msgstr "A lista de endereços já está carregada."

#: ../src/e-contact-entry.c:842
msgid "Permission was denied when accessing the address book."
msgstr "Permissão negada ao tentar aceder à lista de endereços."

#: ../src/e-contact-entry.c:844
msgid "The contact was not found."
msgstr "O contacto não foi encontrado."

#: ../src/e-contact-entry.c:846
msgid "This contact ID already exists."
msgstr "O ID deste contacto já existe."

#: ../src/e-contact-entry.c:848
msgid "The protocol is not supported."
msgstr "O protocolo não é suportado."

#: ../src/e-contact-entry.c:850
msgid "The operation was cancelled."
msgstr "A operação foi cancelada."

#: ../src/e-contact-entry.c:852
msgid "The operation could not be cancelled."
msgstr "A operação não pode ser cancelada."

#: ../src/e-contact-entry.c:854
msgid "The address book authentication failed."
msgstr "A autenticação da lista de endereços falhou."

#: ../src/e-contact-entry.c:856
msgid ""
"Authentication is required to access the address book and was not given."
msgstr ""
"É requerida autenticação para aceder à lista de endereços e não foi dada."

#: ../src/e-contact-entry.c:858
msgid "A secure connection is not available."
msgstr "Não está disponível uma ligação segura."

#: ../src/e-contact-entry.c:860
msgid "A CORBA error occurred whilst accessing the address book."
msgstr "Ocorreu um erro CORBA ao aceder à lista de endereços."

#: ../src/e-contact-entry.c:862
msgid "The address book source does not exist."
msgstr "A fonte da lista de endereços não existe."

#: ../src/e-contact-entry.c:864 ../src/e-contact-entry.c:867
msgid "An unknown error occurred."
msgstr "Ocorreu um erro desconhecido."

#: ../src/glade-utils.c:49
#, c-format
msgid "Couldn't find necessary glade file '%s'"
msgstr "Não foi possível encontrar o ficheiro glade necessário '%s'"

#: ../src/glade-utils.c:59
#, c-format
msgid "Glade file '%s' is missing widget '%s'."
msgstr "O ficheiro glade '%s' foi incapaz de encontrar o widget '%s'."

#: ../src/glade-utils.c:139
#, c-format
msgid "Glade file is missing widget '%s'"
msgstr "O ficheiro glade foi incapaz de encontrar o widget '%s'."
