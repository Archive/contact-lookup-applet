# Serbian translation of PACKAGE
# Courtesy of Prevod.org team (http://prevod.org/) -- 2003, 2004.
#
# This file is distributed under the same license as the PACKAGE package.
#
# Maintainer: Данило Шеган <dsegan@gmx.net>
#
msgid ""
msgstr ""
"Project-Id-Version: contact-lookup-applet\n"
"PO-Revision-Date: 2005-01-30 13:04+0100\n"
"Last-Translator: Данило Шеган <danilo@prevod.org>\n"
"Language-Team: Serbian (sr) <gnom@prevod.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"


#: data/GNOME_ContactLookupApplet.server.in.h:1
msgid "Address Book Search"
msgstr "Претрага адресара"

#: data/GNOME_ContactLookupApplet.server.in.h:2
msgid "Search for a person in your address book"
msgstr "Нађите особу у вашем адресару"

#: data/GNOME_ContactLookupApplet.xml.h:1
msgid "_About..."
msgstr "_О програму..."

#: data/GNOME_ContactLookupApplet.xml.h:2
msgid "_Help"
msgstr "_Помоћ"

#: data/contact-lookup-applet.glade.h:1
msgid "<b>\"Foo\" is online</b>"
msgstr "<b>„Фу“ је на вези</b>"

#: data/contact-lookup-applet.glade.h:2
msgid "<b>Electronic Mail</b>"
msgstr "<b>Електронска пошта</b>"

#: data/contact-lookup-applet.glade.h:3
msgid "<b>Instant Messaging</b>"
msgstr "<b>Брзе поруке</b>"

#: data/contact-lookup-applet.glade.h:4
msgid "<b>Video Conferencing</b>"
msgstr "<b>Видео разговор</b>"

#: data/contact-lookup-applet.glade.h:5
msgid "ADDRESS"
msgstr "АДРЕСА"

#: data/contact-lookup-applet.glade.h:6
msgid "Address Card"
msgstr "Визит картица"

#: data/contact-lookup-applet.glade.h:7
msgid "Compose _Mail Message\t"
msgstr "Састави _поштанску поруку"

#: data/contact-lookup-applet.glade.h:8
msgid "HOME PAGE"
msgstr "ЛИЧНА СТРАНА"

#: data/contact-lookup-applet.glade.h:9
msgid "NAME"
msgstr "ИМЕ"

#: data/contact-lookup-applet.glade.h:10
msgid "PHONE"
msgstr "ТЕЛЕФОН"

#: data/contact-lookup-applet.glade.h:11
msgid "Send _Instant Message"
msgstr "Пошаљи _брзу поруку"

#: data/contact-lookup-applet.glade.h:12
msgid "Start _Video Conference"
msgstr "Започни _видео разговор"

#: data/contact-lookup-applet.glade.h:13
msgid "VIDEO"
msgstr "ВИДЕО"

#: data/contact-lookup-applet.glade.h:14
msgid "_Edit Contact Information"
msgstr "_Уреди податке контакта"

#: data/contact-lookup-applet.glade.h:15
msgid "_Go"
msgstr "_Иди"

# bug: don't use markup
#: src/contact-dialog.c:53
#, c-format
msgid ""
"<span weight=\"bold\" size=\"large\">Cannot show URL %s:</span>\n"
"\n"
"%s"
msgstr ""
"<span weight=\"bold\" size=\"large\">Не могу да прикажем адресу %s:</span>\n"
"\n"
"%s"

# bug: don't use markup
#: src/contact-dialog.c:81
#, c-format
msgid ""
"<span weight=\"bold\" size=\"large\">Cannot send email to %s:</span>\n"
"\n"
"%s"
msgstr ""
"<span weight=\"bold\" size=\"large\">Не могу да пошаљем е-писмо на %s:</span>\n"
"\n"
"%s"

# bug: don't use markup
#: src/contact-dialog.c:100
#, c-format
msgid ""
"<span weight=\"bold\" size=\"large\">Cannot start conference with %s:</"
"span>\n"
"\n"
"%s"
msgstr ""
"<span weight=\"bold\" size=\"large\">Не могу да почнем видео разговор са %s:</span>\n"
"\n"
"%s"

# bug: string composition
#: src/contact-dialog.c:321
msgid " (home)\n"
msgstr " (кућни)\n"

# bug: string composition
#: src/contact-dialog.c:327
msgid " (work)\n"
msgstr " (пословни)\n"

# bug: string composition
#: src/contact-dialog.c:333
msgid " (mobile)\n"
msgstr " (мобилни)\n"

#: src/contact-lookup-applet.c:59
#, c-format
msgid "Unable to search your address book: %s"
msgstr "Не могу да претражујем твој адресар: %s"

#: src/contact-lookup-applet.c:63
msgid "Search your address book"
msgstr "Претражите адресар"

# bug: mark-up
#: src/contact-lookup-applet.c:95
#, c-format
msgid ""
"<span size='larger' weight='bold'>Error accessing addressbook</span>\n"
"\n"
"%s"
msgstr ""
"<span size='larger' weight='bold'>Грешка при приступању адресару</span>\n"
"\n"
"%s"

#: src/contact-lookup-applet.c:112
msgid "An applet to search your address book."
msgstr "Програмче за претрагу вашег адресара."

# bug: mark-up
#: src/contact-lookup-applet.c:159
msgid ""
"<span size='larger' weight='bold'>No Address Books Available</span>\n"
"\n"
"There are no address books selected for auto-completion. To select which "
"address books to use for completion, go to Tools -> Settings -> "
"Autocompletion in Evolution."
msgstr ""
"<span size='larger' weight='bold'>Нема доступних адресара</span>\n"
"\n"
"Нису одабрани адресари за самодопуњавање. Да изаберете које адресаре користити за допуњавање, идите на Алати -> Подешавања -> Самодопуњавање у Еволуцији."

#: src/e-contact-entry.c:149
#, c-format
msgid "Cannot get contact: %s"
msgstr "Не могу да ишчитам контакт: %s"

#: src/e-contact-entry.c:182
#, c-format
msgid "Could not find contact: %s"
msgstr "Не могу да нађем контакт: %s"

#: src/e-contact-entry.c:358
msgid "Cannot create searchable view."
msgstr "Не могу да направи претраживи преглед."

#: src/e-contact-entry.c:737
msgid "Success"
msgstr "Успешно"

#: src/e-contact-entry.c:739
msgid "An argument was invalid."
msgstr "Аргумент је неисправан."

#: src/e-contact-entry.c:741
msgid "The address book is busy."
msgstr "Адресар је заузет."

#: src/e-contact-entry.c:743
msgid "The address book is offline."
msgstr "Адресар је недоступан."

#: src/e-contact-entry.c:745
msgid "The address book does not exist."
msgstr "Адресар не постоји."

#: src/e-contact-entry.c:747
msgid "The \"Me\" contact does not exist."
msgstr "Контакт „ја“ не постоји."

#: src/e-contact-entry.c:749
msgid "The address book is not loaded."
msgstr "Адресар није учитан."

#: src/e-contact-entry.c:751
msgid "The address book is already loaded."
msgstr "Адресар је већ учитан."

#: src/e-contact-entry.c:753
msgid "Permission was denied when accessing the address book."
msgstr "Одбијен је приступ адресару."

#: src/e-contact-entry.c:755
msgid "The contact was not found."
msgstr "Контакт није нађен."

#: src/e-contact-entry.c:757
msgid "This contact ID already exists."
msgstr "Овај ИБ контакта већ постоји."

#: src/e-contact-entry.c:759
msgid "The protocol is not supported."
msgstr "Протокол није подржан."

#: src/e-contact-entry.c:761
msgid "The operation was cancelled."
msgstr "Операција је отказана."

#: src/e-contact-entry.c:763
msgid "The operation could not be cancelled."
msgstr "Операција се не може отказати."

#: src/e-contact-entry.c:765
msgid "The address book authentication failed."
msgstr "Неуспешна идентификација адресара."

#: src/e-contact-entry.c:767
msgid ""
"Authentication is required to access the address book and was not given."
msgstr "Неопходна је идентификација за приступ адресару, а није дата."

#: src/e-contact-entry.c:769
msgid "A secure connection is not available."
msgstr "Безбедна веза није доступна."

#: src/e-contact-entry.c:771
msgid "A CORBA error occured whilst accessing the address book."
msgstr "Дошло је до CORBA грешке при приступу адресару."

#: src/e-contact-entry.c:773
msgid "The address book source does not exist."
msgstr "Извор адресара не постоји."

#: src/e-contact-entry.c:775 src/e-contact-entry.c:778
msgid "An unknown error occured."
msgstr "Догодила се непозната грешка."

#: src/glade-utils.c:49
#, c-format
msgid "Couldn't find necessary glade file '%s'"
msgstr "Не могу да нађем неопходну глејд датотеку „%s“"

#: src/glade-utils.c:59
#, c-format
msgid "Glade file '%s' is missing widget '%s'."
msgstr "У глејд датотеци „%s“ недостаје елемент „%s“."

#: src/glade-utils.c:139
#, c-format
msgid "Glade file is missing widget '%s'"
msgstr "У глејд датотеци недостаје елемент „%s“"
