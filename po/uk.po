# Ukrainian translation of contact-lookup-applet. 
# Copyright (C) 2005 Free Software Foundation, Inc. 
# Maxim Dziumanenko <mvd@mylinux.ua>, 2005 
msgid ""
msgstr ""
"Project-Id-Version:  contact-lookup-applet\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2005-08-09 13:20+0200\n"
"PO-Revision-Date: 2004-08-09 11:46+0300\n"
"Last-Translator: Maxim Dziumanenko <mvd@mylinux.com.ua>\n"
"Language-Team: Ukrainian <uk@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%"
"10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: ../data/GNOME_ContactLookupApplet.server.in.in.h:1
msgid "Address Book Search"
msgstr "Пошук у адресній книзі"

#: ../data/GNOME_ContactLookupApplet.server.in.in.h:2
msgid "Search for a person in your address book"
msgstr "Пошук особи в вашій адресній книзі"

#: ../data/GNOME_ContactLookupApplet.xml.h:1
msgid "_About..."
msgstr "_Про програму..."

#: ../data/GNOME_ContactLookupApplet.xml.h:2
msgid "_Help"
msgstr "_Довідка"

#: ../data/contact-lookup-applet.glade.h:1
msgid "<b>\"Foo\" is online</b>"
msgstr "<b>\"Foo\" у мережі</b>"

#: ../data/contact-lookup-applet.glade.h:2
msgid "<b>Electronic Mail</b>"
msgstr "<b>Електронна пошта</b>"

#: ../data/contact-lookup-applet.glade.h:3
msgid "<b>Instant Messaging</b>"
msgstr "<b>Миттєві повідомлення</b>"

#: ../data/contact-lookup-applet.glade.h:4
msgid "<b>Video Conferencing</b>"
msgstr "<b>Відео-конференції</b>"

#: ../data/contact-lookup-applet.glade.h:5
msgid "ADDRESS"
msgstr "АДРЕСА"

#: ../data/contact-lookup-applet.glade.h:6
msgid "Address Card"
msgstr "Адресна картка"

#: ../data/contact-lookup-applet.glade.h:7
msgid "Compose _Mail Message\t"
msgstr "Створити _поштове повідомлення\t"

#: ../data/contact-lookup-applet.glade.h:8
msgid "HOME PAGE"
msgstr "ДОМАШНЯ СТОРІНКА"

#: ../data/contact-lookup-applet.glade.h:9
msgid "NAME"
msgstr "НАЗВА"

#: ../data/contact-lookup-applet.glade.h:10
msgid "PHONE"
msgstr "ТЕЛЕФОН"

#: ../data/contact-lookup-applet.glade.h:11
msgid "Send _Instant Message"
msgstr "Надіслати _миттєве повідомлення"

#: ../data/contact-lookup-applet.glade.h:12
msgid "Start _Video Conference"
msgstr "Запустити _відео-конференцію"

#: ../data/contact-lookup-applet.glade.h:13
msgid "VIDEO"
msgstr "ВІДЕО"

#: ../data/contact-lookup-applet.glade.h:14
msgid "_Edit Contact Information"
msgstr "_Правка інформації про контакт"

#: ../data/contact-lookup-applet.glade.h:15
msgid "_Go"
msgstr "Пере_йти"

#: ../src/contact-dialog.c:53
#, c-format
msgid ""
"<span weight=\"bold\" size=\"large\">Cannot show URL %s:</span>\n"
"\n"
"%s"
msgstr ""
"<span weight=\"bold\" size=\"large\">Не вдається показати URL %s:</span>\n"
"\n"
"%s"

#: ../src/contact-dialog.c:81
#, c-format
msgid ""
"<span weight=\"bold\" size=\"large\">Cannot send email to %s:</span>\n"
"\n"
"%s"
msgstr ""
"<span weight=\"bold\" size=\"large\">Не вдається надіслати ел.пошту до %s:</span>\n"
"\n"
"%s"

#: ../src/contact-dialog.c:100
#, c-format
msgid ""
"<span weight=\"bold\" size=\"large\">Cannot start conference with %s:</"
"span>\n"
"\n"
"%s"
msgstr ""
"<span weight=\"bold\" size=\"large\">Не вдається запустити конференцію з %s:</"
"span>\n"
"\n"
"%s"

#: ../src/contact-dialog.c:321
msgid " (home)\n"
msgstr " (домашній)\n"

#: ../src/contact-dialog.c:327
msgid " (work)\n"
msgstr " (робочий)\n"

#: ../src/contact-dialog.c:333
msgid " (mobile)\n"
msgstr " (мобільний)\n"

#: ../src/contact-lookup-applet.c:59
#, c-format
msgid "Unable to search your address book: %s"
msgstr "Не вдається знайти вашу адресну книгу: %s"

#: ../src/contact-lookup-applet.c:63
msgid "Search your address book"
msgstr "Пошук адресної книги"

#: ../src/contact-lookup-applet.c:95
#, c-format
msgid ""
"<span size='larger' weight='bold'>Error accessing addressbook</span>\n"
"\n"
"%s"
msgstr ""
"<span size='larger' weight='bold'>Помилка доступу до адресної книги</span>\n"
"\n"
"%s"

#: ../src/contact-lookup-applet.c:126
msgid "An applet to search your address book."
msgstr "Аплет пошуку у адресній книзі."

#: ../src/contact-lookup-applet.c:138
msgid ""
"<span size='larger' weight='bold'>No Address Books Available</span>\n"
"\n"
"There are no address books selected for auto-completion. To select which "
"address books to use for completion, go to Edit -> Preferences -> "
"Autocompletion in Evolution."
msgstr ""
"<span size='larger' weight='bold'>Адресна книга недоступна</span>\n"
"\n"
"Не виділено адресних книг для автоматичного доповнення. Щоб вибрати "
"адресну книгу для доповнення, перейдіть Правка -> Параметри -> "
"Автодоповнення у Evolution."

#: ../src/e-contact-entry.c:152
#, c-format
msgid "Cannot get contact: %s"
msgstr "Не вдається отримати контакт: %s"

#: ../src/e-contact-entry.c:184
#, c-format
msgid "Could not find contact: %s"
msgstr "Не вдається знайти контакт: %s"

#: ../src/e-contact-entry.c:366
msgid "Cannot create searchable view."
msgstr "Не вдається створити вікно пошуку."

#: ../src/e-contact-entry.c:810
msgid "Success"
msgstr "Успішно"

#: ../src/e-contact-entry.c:812
msgid "An argument was invalid."
msgstr "Неправильний аргумент."

#: ../src/e-contact-entry.c:814
msgid "The address book is busy."
msgstr "Адресна книга зайнята."

#: ../src/e-contact-entry.c:816
msgid "The address book is offline."
msgstr "Адресна книга не у мережі."

#: ../src/e-contact-entry.c:818
msgid "The address book does not exist."
msgstr "Адресна книга не існує."

#: ../src/e-contact-entry.c:820
msgid "The \"Me\" contact does not exist."
msgstr "Контакт \"Me\" не існує."

#: ../src/e-contact-entry.c:822
msgid "The address book is not loaded."
msgstr "Адресна книга не завантажена."

#: ../src/e-contact-entry.c:824
msgid "The address book is already loaded."
msgstr "Адресна книга вже завантажена."

#: ../src/e-contact-entry.c:826
msgid "Permission was denied when accessing the address book."
msgstr "Доступ заборонено при спробі доступу до адресної книги."

#: ../src/e-contact-entry.c:828
msgid "The contact was not found."
msgstr "Контакт не знайдено."

#: ../src/e-contact-entry.c:830
msgid "This contact ID already exists."
msgstr "Контакт вже існує."

#: ../src/e-contact-entry.c:832
msgid "The protocol is not supported."
msgstr "Протокол не підтримується."

#: ../src/e-contact-entry.c:834
msgid "The operation was cancelled."
msgstr "Операцію скасовано."

#: ../src/e-contact-entry.c:836
msgid "The operation could not be cancelled."
msgstr "Операція не може бути скасована. "

#: ../src/e-contact-entry.c:838
msgid "The address book authentication failed."
msgstr "Помилка аутентифікації адресної книги."

#: ../src/e-contact-entry.c:840
msgid ""
"Authentication is required to access the address book and was not given."
msgstr ""
"для доступу до адресної книги потрібна аутентифікація."

#: ../src/e-contact-entry.c:842
msgid "A secure connection is not available."
msgstr "Безпечне з'єднання недоступне."

#: ../src/e-contact-entry.c:844
msgid "A CORBA error occured whilst accessing the address book."
msgstr "При доступі до адресної книги виникла помилка у системі CORBA."

#: ../src/e-contact-entry.c:846
msgid "The address book source does not exist."
msgstr "Джерело адресної книги не існує."

#: ../src/e-contact-entry.c:848 ../src/e-contact-entry.c:851
msgid "An unknown error occured."
msgstr "Невідома помилка."

#: ../src/glade-utils.c:49
#, c-format
msgid "Couldn't find necessary glade file '%s'"
msgstr "Не вдається знайти необхідний файл glade '%s'"

#: ../src/glade-utils.c:59
#, c-format
msgid "Glade file '%s' is missing widget '%s'."
msgstr "У файлі glade '%s' відсутній віджет '%s'."

#: ../src/glade-utils.c:139
#, c-format
msgid "Glade file is missing widget '%s'"
msgstr "У файлі glade відсутній віджет '%s'."
