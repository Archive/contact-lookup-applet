/* 
 * Copyright (C) 2003 Ross Burton <ross@burtonini.com>
 *
 * Contact Lookup Applet
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Ross Burton <ross@burtonini.com>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif /* HAVE_CONFIG_H */

#include "contact-lookup-applet.h"
#include "e-contact-entry.h"
#include "contact-dialog.h"
#include "glade-utils.h"

#include <string.h>
#include <glib/gi18n.h>
#include <bonobo/bonobo-ui-component.h>
#include <panel-applet-gconf.h>
#include <libebook/e-book.h>
#include <libebook/e-book-types.h>
#include <libebook/e-contact.h>
#include <libedataserver/e-source-list.h>

#define GCONF_COMPLETION "/apps/evolution/addressbook"
#define GCONF_COMPLETION_SOURCES GCONF_COMPLETION "/sources"
#define GCONF_COMPLETION_LENGTH GCONF_COMPLETION "/minimum_query_length"

/**
 * Sets the tooltip to the given err_msg or if err_msg is NULL displays the
 * normal tooltip.
 */
static void
set_tooltip (EvoLookupApplet *applet, const gchar *err_msg) {
  g_return_if_fail (applet != NULL);
  
  if (err_msg) {
    GString *msg = g_string_new (NULL);
    g_string_append_printf (msg, _("Unable to search your address book: %s"), err_msg);
    gtk_tooltips_set_tip (applet->tips, applet->applet_widget, msg->str, NULL);
    g_string_free (msg, TRUE);
  } else {
    gtk_tooltips_set_tip (applet->tips, applet->applet_widget, _("Search your address book"), NULL);
  }
}

/**
 * The entry has been activated. Display the contact information for the first
 * entry in the list.
 */
static void
entry_contact_selected_cb (EContactEntry *entry, EContact *contact, const char *identifier, gpointer user_data)
{
  EvoLookupApplet *applet;
  GtkWidget *dialog;

  g_return_if_fail (entry != NULL);
  g_return_if_fail (contact != NULL);
  g_return_if_fail (user_data != NULL);
  applet = (EvoLookupApplet*)user_data;

  /* The contact dialog refs and unrefs the contact */
  dialog = contact_dialog_new (contact, identifier);
  gtk_widget_show (dialog);
}

/**
 * The entry's state has changed, maybe because there's no completion entry
 * or one became available
 */
static void
entry_state_change (EContactEntry *entry, gboolean state, gpointer user_data)
{
  EvoLookupApplet *applet;

  g_return_if_fail (entry != NULL);
  g_return_if_fail (user_data != NULL); 
  applet = (EvoLookupApplet*)user_data;

  gtk_widget_set_sensitive (applet->entry_widget, state);
}

/**
 * An error occured in the EContactEntry, so tell the user.
 */
static void
entry_error_cb (EContactEntry *entry, const char* error) {
  GtkWidget *dialog;
  dialog = gtk_message_dialog_new_with_markup (NULL, 0, 
                                               GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE,
                                               _("<span size='larger' weight='bold'>Error accessing addressbook</span>\n\n%s"), error);
  gtk_dialog_run (GTK_DIALOG (dialog));
  gtk_widget_destroy (dialog);
}

static gboolean
entry_button_press_cb (GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
  EvoLookupApplet *applet;
  g_return_val_if_fail (user_data != NULL, FALSE);
  applet = (EvoLookupApplet*)user_data;
 
  panel_applet_request_focus (PANEL_APPLET (applet->applet_widget), event->time);

  /* We just wanted to insert the above function call before the default action */
  return FALSE;
}

/**
 * About dialog callback.
 */
static void
about_cb (BonoboUIComponent *uic, gpointer user_data, const gchar *verbname)
{
  const char* authors[] = {"Ross Burton <ross@burtonini.com>", NULL};

  gtk_show_about_dialog (NULL,
			 "name", PACKAGE_NAME, 
			 "version", PACKAGE_VERSION,
			 "copyright", "Copyright \xc2\xa9 2003-2004 Ross Burton",
			 "comments", _("An applet to search your address book."),
			 "authors", authors,
			 "logo-icon-name", "stock_contact",
			 NULL);
}

static void
display_no_sources_dialog (void)
{
  GtkWidget *dialog;
  dialog = gtk_message_dialog_new_with_markup (NULL, 0, GTK_MESSAGE_ERROR,
                                               GTK_BUTTONS_CLOSE,
                                               _("<span size='larger' weight='bold'>No Address Books Available</span>\n\n"
                                                 "There are no address books selected for auto-completion. "
                                                 "To select which address books to use for completion, go to Edit -> Preferences -> Autocompletion in Evolution."));
  /* TODO: better message */
  gtk_dialog_run (GTK_DIALOG (dialog));
  gtk_widget_destroy (dialog);
}

static void
source_list_changed_cb (ESourceList *source_list, EvoLookupApplet *applet)
{
  if (applet->source_list != NULL) {
    g_object_unref (applet->source_list);
    applet->source_list = NULL;
  }

  applet->source_list = e_source_list_new_for_gconf (applet->gconf_client, GCONF_COMPLETION_SOURCES);
  e_contact_entry_set_source_list (E_CONTACT_ENTRY (applet->entry_widget),
      applet->source_list);
  if (applet->source_list != NULL && e_source_list_peek_groups (applet->source_list) != NULL) {
    g_signal_connect (G_OBJECT (applet->source_list), "changed",
	G_CALLBACK (source_list_changed_cb), applet);
  } else {
    display_no_sources_dialog ();
  }
}

/**
 * Panel callback, the size of the applet has changed.
 */
static void
change_size_cb (PanelApplet *widget, gint size, EvoLookupApplet *applet)
{
  theme_image (GTK_IMAGE (applet->icon), "stock_contact", size);
}

static void
change_background_cb (PanelApplet *applet,
		      PanelAppletBackgroundType type,
		      GdkColor *colour,
		      GdkPixmap *pixmap)
{
	GtkRcStyle *rc_style;
	GtkStyle *style;

	/* reset style */
	gtk_widget_set_style (GTK_WIDGET (applet), NULL);
	rc_style = gtk_rc_style_new ();
	gtk_widget_modify_style (GTK_WIDGET (applet), rc_style);
	g_object_unref (rc_style);

	switch (type) {
	case PANEL_NO_BACKGROUND:
		break;
	case PANEL_COLOR_BACKGROUND:
		gtk_widget_modify_bg (GTK_WIDGET (applet),
				      GTK_STATE_NORMAL, colour);
		break;
	case PANEL_PIXMAP_BACKGROUND:
		style = gtk_style_copy (gtk_widget_get_style (GTK_WIDGET (applet)));
		if (style->bg_pixmap[GTK_STATE_NORMAL])
			g_object_unref (style->bg_pixmap[GTK_STATE_NORMAL]);
		style->bg_pixmap[GTK_STATE_NORMAL] = g_object_ref (pixmap);
		gtk_widget_set_style (GTK_WIDGET (applet), style);
		break;
	}
}

/**
 * Callback from Bonobo when the control is destroyed.
 */
static void
bonobo_destroy_cb (BonoboObject *object, EvoLookupApplet *applet)
{
  g_object_unref (applet->gconf_client);
  g_object_unref (applet->source_list);
  g_free (applet);
}

/**
 * The BonoboUI verb->callback table.
 */
static const BonoboUIVerb evo_lookup_applet_menu_verbs [] = {
  BONOBO_UI_VERB ("about", about_cb),
  /* TODO: add Help */
  BONOBO_UI_VERB_END
};

/**
 * Create a new instance of the applet.
 */
static gboolean
evo_lookup_applet_new (PanelApplet *parent_applet)
{
  EvoLookupApplet *applet;
  GtkWidget *hbox;
  
  applet = g_new0 (EvoLookupApplet, 1);
  applet->applet_widget = GTK_WIDGET (parent_applet);

  applet->tips = gtk_tooltips_new ();
  set_tooltip (applet, NULL);
  
  hbox = gtk_hbox_new (FALSE, 0);  

  applet->icon = gtk_image_new ();
  theme_image (GTK_IMAGE (applet->icon), "stock_contact", panel_applet_get_size(parent_applet));
  gtk_box_pack_start (GTK_BOX (hbox), applet->icon, FALSE, FALSE, 0);

  applet->entry_widget = e_contact_entry_new ();
  gtk_widget_set_sensitive (applet->entry_widget, FALSE);
  gtk_entry_set_width_chars (GTK_ENTRY (applet->entry_widget), 15);
  g_signal_connect (applet->entry_widget, "contact-selected", G_CALLBACK (entry_contact_selected_cb), applet);
  g_signal_connect (applet->entry_widget, "error", G_CALLBACK (entry_error_cb), applet);
  g_signal_connect (applet->entry_widget, "button_press_event", G_CALLBACK (entry_button_press_cb), applet);
  g_signal_connect (applet->entry_widget, "state-change", G_CALLBACK (entry_state_change), applet);
  gtk_box_pack_start (GTK_BOX (hbox), applet->entry_widget, TRUE, TRUE, 0);
  
  gtk_widget_show_all (hbox);
  
  gtk_container_add (GTK_CONTAINER (parent_applet), hbox);
  
  panel_applet_setup_menu_from_file (PANEL_APPLET (applet->applet_widget),
                                     NULL,
                                     PKGDATADIR"/GNOME_ContactLookupApplet.xml",
                                     NULL,
                                     evo_lookup_applet_menu_verbs,
                                     applet);

  gtk_widget_show (applet->applet_widget);
  
  g_signal_connect (G_OBJECT (applet->applet_widget), "change_size",
                    G_CALLBACK (change_size_cb), applet);
  g_signal_connect (G_OBJECT (applet->applet_widget), "change_background",
		    G_CALLBACK (change_background_cb), applet);
  g_signal_connect (panel_applet_get_control (PANEL_APPLET (applet->applet_widget)), "destroy",
                    G_CALLBACK (bonobo_destroy_cb), applet);
  applet->gconf_client = gconf_client_get_default ();
  /* Monitor the Evolution completion preferences */
  gconf_client_add_dir (applet->gconf_client, GCONF_COMPLETION, GCONF_CLIENT_PRELOAD_ONELEVEL, NULL);
  /* Add a callback on source changes */
  source_list_changed_cb (NULL, applet);
  /* TODO: if used in evo, monitor .../minimum_query_length and set as relevant on EContactEntry */

  return TRUE;
}

/**
 * The entry point for this factory. If the OAFIID matches, create an instance
 * of the applet.
 */
static gboolean
evo_lookup_applet_factory (PanelApplet *applet, const gchar *iid, gpointer data)
{
  if (!strcmp (iid, "OAFIID:GNOME_ContactLookupApplet")) {
    return evo_lookup_applet_new (applet);
  }
  return FALSE;
}

/**
 * Generate the boilerplate to hook into GObject/Bonobo.
 */
PANEL_APPLET_BONOBO_FACTORY ("OAFIID:GNOME_ContactLookupApplet_Factory",
                             PANEL_TYPE_APPLET,
                             PACKAGE_NAME, PACKAGE_VERSION,
                             evo_lookup_applet_factory,
                             NULL);
