/* 
 * Copyright (C) 2003 Ross Burton <ross@burtonini.com>
 *
 * Contact Lookup Applet
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Ross Burton <ross@burtonini.com>
 */

#ifndef EVO_LOOKUP_APPLET_H
#define EVO_LOOKUP_APPLET_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif /* HAVE_CONFIG_H */

#include <panel-applet.h>
#include <gconf/gconf-client.h>
#include <gtk/gtk.h>
#include "e-contact-entry.h"

typedef struct _EvoLookupApplet {
  /* GConf */
  GConfClient *gconf_client;
  ESourceList *source_list;

  /* Widgets */
  GtkWidget *applet_widget;
  GtkWidget *icon;
  GtkWidget *entry_widget;
  GtkTooltips *tips;
} EvoLookupApplet;

#endif /* EVO_LOOKUP_APPLET_H */
