/* 
 * Copyright (C) 2003 Ross Burton <ross@burtonini.com>
 *
 * Contact Lookup Applet
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Ross Burton <ross@burtonini.com>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif /* HAVE_CONFIG_H */

#include <glib/gi18n.h>
#include <gtk/gtk.h>

/*
 * This code is taken from Gossip, (C) 2003 Imendio.
 */

static GtkBuilder *
get_glade_file (const gchar *filename,
		const gchar *first_required_widget, va_list args)
{
	GtkBuilder *gui;
	const char *name;
	GObject   **widget_ptr;
	GError     *err = NULL;

	gui = gtk_builder_new ();
	gtk_builder_set_translation_domain (gui, GETTEXT_PACKAGE);
	if (gtk_builder_add_from_file (gui, filename, &err) == 0) {
		g_warning (_("UI file error: %s"), err->message);
		g_error_free (err);
		return NULL;
	}

	for (name = first_required_widget; name; name = va_arg (args, char *)) {
		widget_ptr = va_arg (args, void *);
		
		*widget_ptr = gtk_builder_get_object (gui, name);
		
		if (!*widget_ptr) {
			g_warning (_("UI file '%s' is missing widget '%s'."),
				   filename, name);
			continue;
		}
	}
	
	return gui;
}

void
utils_glade_get_file_simple (const gchar *filename,
			     const gchar *first_required_widget, ...)
{
	va_list     args;
	GtkBuilder *gui;

	va_start (args, first_required_widget);

	gui = get_glade_file (filename,
			      first_required_widget,
			      args);
	
	va_end (args);

	if (!gui) {
		return;
	}

	g_object_unref (gui);
}

GtkBuilder *
utils_glade_get_file (const gchar *filename,
		      const gchar *first_required_widget, ...)
{
	va_list     args;
	GtkBuilder *gui;

	va_start (args, first_required_widget);

	gui = get_glade_file (filename,
			      first_required_widget,
			      args);
	
	va_end (args);

	if (!gui) {
		return NULL;
	}

	return gui;
}

void
utils_glade_setup_size_group (GtkBuilder        *gui,
			      GtkSizeGroupMode  mode,
			      gchar            *first_widget, ...)
{
	va_list       args;
	GObject      *widget;
	GtkSizeGroup *size_group;
	const gchar  *name;

	va_start (args, first_widget);

	size_group = gtk_size_group_new (mode);
	
	for (name = first_widget; name; name = va_arg (args, char *)) {
		widget = gtk_builder_get_object (gui, name);
		if (!widget) {
			g_warning (_("UI file is missing widget '%s'"), name);
			continue;
		}

		gtk_size_group_add_widget (size_group, GTK_WIDGET (widget));
	}

	g_object_unref (size_group);

	va_end (args);
}

GdkPixbuf*
get_icon (const char* icon_name, int size)
{
  static GtkIconTheme *theme = NULL;
  GError *error = NULL;
  GdkPixbuf *pixbuf;

  g_return_val_if_fail (icon_name != NULL, NULL);
  
  if (theme == NULL) {
    theme = gtk_icon_theme_get_default ();
    g_assert (theme != NULL);
  }
  pixbuf = gtk_icon_theme_load_icon (theme, icon_name, size, 0, &error);
  if (error) {
    g_warning ("%s: %s", __FUNCTION__, error->message);
    g_error_free (error);
    return NULL;
  }
  return pixbuf;
}

void
theme_image (GtkImage *image, const char* icon_name, int size)
{
  GdkPixbuf *pixbuf;

  g_return_if_fail (image != NULL);
  g_return_if_fail (icon_name != NULL);
  
  pixbuf = get_icon (icon_name, size);
  if (pixbuf != NULL) {
    gtk_image_set_from_pixbuf (image, pixbuf);
    g_object_unref (pixbuf);
  }
}
