/* 
 * Copyright (C) 2003 Ross Burton <ross@burtonini.com>
 *
 * Contact Lookup Applet
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Ross Burton <ross@burtonini.com>
 */

#ifndef GLADE_UTILS_H
#define GLADE_UTILS_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif /* HAVE_CONFIG_H */

/*
 * This code is taken from Gossip, (C) 2003 Imendio.
 */

#include <gtk/gtk.h>

void utils_glade_get_file_simple (const gchar *filename,
                                  const gchar *first_required_widget,
                                  ...);

GtkBuilder *utils_glade_get_file (const gchar *filename,
				  const gchar *first_required_widget,
				  ...);

void utils_glade_setup_size_group (GtkBuilder *gui, GtkSizeGroupMode mode,
                                   gchar *first_widget, ...);

GdkPixbuf* get_icon (const char* icon_name, GtkIconSize size);

void theme_image (GtkImage *image, const char* icon_name, GtkIconSize size);

#endif /* GLADE_UTILS_H */
