/* 
 * Copyright (C) 2004 Ross Burton <ross@burtonini.com>
 *
 * Contact Lookup Applet
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Ross Burton <ross@burtonini.com>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif /* HAVE_CONFIG_H */

#include "contact-fields.h"

#include <glib/gi18n.h>
#include <libebook/e-contact.h>

EContactInfo contactinfo_address[] = {
  {E_CONTACT_ADDRESS_LABEL_HOME, NULL},
  {E_CONTACT_ADDRESS_LABEL_WORK, NULL},
  {E_CONTACT_ADDRESS_LABEL_OTHER, NULL},
  {-1, NULL}
};

const char *contactinfo_address_names[] = {
	N_("Home"),
	N_("Work"),
	N_("Other")
};

EContactInfo contactinfo_im[] = {
  {E_CONTACT_IM_AIM_HOME_1, "im-aim"},
  {E_CONTACT_IM_AIM_HOME_2, "im-aim"},
  {E_CONTACT_IM_AIM_HOME_3, "im-aim"},
  {E_CONTACT_IM_AIM_WORK_1, "im-aim"},
  {E_CONTACT_IM_AIM_WORK_2, "im-aim"},
  {E_CONTACT_IM_AIM_WORK_3, "im-aim"},
  {E_CONTACT_IM_GROUPWISE_HOME_1, "im-groupwise"},
  {E_CONTACT_IM_GROUPWISE_HOME_2, "im-groupwise"},
  {E_CONTACT_IM_GROUPWISE_HOME_3, "im-groupwise"},
  {E_CONTACT_IM_GROUPWISE_WORK_1, "im-groupwise"},
  {E_CONTACT_IM_GROUPWISE_WORK_2, "im-groupwise"},
  {E_CONTACT_IM_GROUPWISE_WORK_3, "im-groupwise"},
  {E_CONTACT_IM_JABBER_HOME_1, "im-jabber"},
  {E_CONTACT_IM_JABBER_HOME_2, "im-jabber"},
  {E_CONTACT_IM_JABBER_HOME_3, "im-jabber"},
  {E_CONTACT_IM_JABBER_WORK_1, "im-jabber"},
  {E_CONTACT_IM_JABBER_WORK_2, "im-jabber"},
  {E_CONTACT_IM_JABBER_WORK_3, "im-jabber"},
  {E_CONTACT_IM_YAHOO_HOME_1, "im-yahoo"},
  {E_CONTACT_IM_YAHOO_HOME_2, "im-yahoo"},
  {E_CONTACT_IM_YAHOO_HOME_3, "im-yahoo"},
  {E_CONTACT_IM_YAHOO_WORK_1, "im-yahoo"},
  {E_CONTACT_IM_YAHOO_WORK_2, "im-yahoo"},
  {E_CONTACT_IM_YAHOO_WORK_3, "im-yahoo"},
  {E_CONTACT_IM_MSN_HOME_1, "im-msn"},
  {E_CONTACT_IM_MSN_HOME_2, "im-msn"},
  {E_CONTACT_IM_MSN_HOME_3, "im-msn"},
  {E_CONTACT_IM_MSN_WORK_1, "im-msn"},
  {E_CONTACT_IM_MSN_WORK_2, "im-msn"},
  {E_CONTACT_IM_MSN_WORK_3, "im-msn"},
  {E_CONTACT_IM_ICQ_HOME_1, "im-icq"},
  {E_CONTACT_IM_ICQ_HOME_2, "im-icq"},
  {E_CONTACT_IM_ICQ_HOME_3, "im-icq"},
  {E_CONTACT_IM_ICQ_WORK_1, "im-icq"},
  {E_CONTACT_IM_ICQ_WORK_2, "im-icq"},
  {E_CONTACT_IM_ICQ_WORK_3, "im-icq"},
  {-1, NULL}
};

EContactInfo contactinfo_email[] = {
  {E_CONTACT_EMAIL_1, NULL},
  {E_CONTACT_EMAIL_2, NULL},
  {E_CONTACT_EMAIL_3, NULL},
  {E_CONTACT_EMAIL_4, NULL},
  {-1, NULL}
};
