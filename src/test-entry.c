
#include <gtk/gtk.h>
#include <string.h>
#include <libedataserver/e-source-list.h>
#include "e-contact-entry.h"

#define GCONF_COMPLETION "/apps/evolution/addressbook"
#define GCONF_COMPLETION_SOURCES GCONF_COMPLETION "/sources"

#define CONTACT_FORMAT "%s (%s)"

static char *text = NULL;
static char *phone_number = NULL;
static GtkWidget *activate;

static void
add_sources (EContactEntry *entry)
{
	ESourceList *source_list;

	source_list =
		e_source_list_new_for_gconf_default (GCONF_COMPLETION_SOURCES);
	e_contact_entry_set_source_list (E_CONTACT_ENTRY (entry),
			source_list);
	g_object_unref (source_list);
}

static void
sources_changed_cb (GConfClient *client, guint cnxn_id,
		GConfEntry *entry, EContactEntry *entry_widget)
{
	add_sources (entry_widget);
}

static void
setup_source_changes (EContactEntry *entry)
{
	GConfClient *gc;

	gc = gconf_client_get_default ();
	gconf_client_add_dir (gc, GCONF_COMPLETION,
			GCONF_CLIENT_PRELOAD_ONELEVEL, NULL);
	gconf_client_notify_add (gc, GCONF_COMPLETION,
			(GConfClientNotifyFunc) sources_changed_cb,
			entry, NULL, NULL);
}

static char *
remove_spaces (const char *str)
{
	GString *nospace;
	char *p;

	p = (char *) str;
	nospace = g_string_new ("");
	while (*p != '\0') {
		gunichar c;
		c = g_utf8_get_char (p);
		if (g_unichar_isspace (c) == FALSE)
			nospace = g_string_append_unichar (nospace, c);
		p = g_utf8_next_char(p);
	}

	return g_string_free (nospace, FALSE);
}

static void
text_changed (GtkEditable *entry, gpointer user_data)
{
	char *current;
	char *p;

	current = g_strdup (gtk_entry_get_text (GTK_ENTRY (entry)));
	if (text == NULL) {
		text = g_strdup (current);
		return;
	}

	if (strcmp (text, current) == 0) {
		g_free (current);
		return;
	}
	text = g_strdup (current);
	if (phone_number != NULL) {
		g_free (phone_number);
		phone_number = NULL;
	}
	p = current;

	while (*p != '\0') {
		gunichar c;
		c = g_utf8_get_char (p);
		/* We only allow digits, plus signs and spaces in user supplied
		 * phone numbers. */
		if (g_unichar_isdigit (c) == FALSE
				&& g_unichar_isspace (c) == FALSE
				&& c != 0x2B) {
			g_free (current);
			gtk_widget_set_sensitive (activate, FALSE);
			return;
		}
		p = g_utf8_next_char(p);
	}

	/* Remove spaces from the phone number */
	phone_number = remove_spaces (current);
	g_free (current);
	gtk_widget_set_sensitive (activate, TRUE);
}

static void
contact_selected_cb (GtkWidget *entry, EContact *contact, const char *identifier)
{
	char *text;

	text = g_strdup_printf (CONTACT_FORMAT, (char*)e_contact_get_const (contact, E_CONTACT_NAME_OR_ORG), identifier);
	phone_number = remove_spaces (identifier);

	gtk_widget_set_sensitive (activate, TRUE);

	g_signal_handlers_block_by_func (G_OBJECT (entry), text_changed, NULL);
	gtk_entry_set_text (GTK_ENTRY (entry), text);
	g_signal_handlers_unblock_by_func (G_OBJECT (entry), text_changed, NULL);

	g_free (text);
}

static GList *
test_display_func (EContact *contact, gpointer data)
{
	GList *entries = NULL;
	EVCard *card = E_VCARD (contact);
	GList *attrs, *a;

	attrs = e_vcard_get_attributes (card);
	for (a = attrs; a; a = a->next) {
		if (strcmp (e_vcard_attribute_get_name (a->data), EVC_TEL) == 0
		    && e_vcard_attribute_has_type (a->data, "CELL")) {
			GList *phones, *p;

			phones = e_vcard_attribute_get_values (a->data);
			for (p = phones; p; p = p->next) {
				EContactEntyItem *item;

				item = g_new0 (EContactEntyItem, 1);
				item->display_string = g_strdup_printf (CONTACT_FORMAT, (char*) e_contact_get_const (contact, E_CONTACT_NAME_OR_ORG), (char*) p->data);
				item->identifier = g_strdup (p->data);
				entries = g_list_prepend (entries, item);
			}
		}
	}

	return g_list_reverse (entries);
}

static gboolean
window_closed (GtkWidget *widget, GdkEvent *event, gpointer data)
{
	g_print ("Phone number selected: %s\n", phone_number);
	exit (0);
	return FALSE;
}

static void
state_change_cb (GtkWidget *entry, gboolean state, gpointer data)
{
	g_print ("State of the entry changed: %s\n", state ? "Enabled" : "Disabled");
	gtk_widget_set_sensitive (entry, state);
}

int main (int argc, char **argv)
{
	GtkWidget *window, *entry;
	EContactField fields[] = { E_CONTACT_FULL_NAME, E_CONTACT_NICKNAME, E_CONTACT_ORG, E_CONTACT_PHONE_MOBILE, 0 };

	gtk_init (&argc, &argv);

	window = gtk_dialog_new ();
	activate = gtk_dialog_add_button (GTK_DIALOG (window), GTK_STOCK_OK, GTK_RESPONSE_OK);
	gtk_widget_set_sensitive (activate, FALSE);
	entry = e_contact_entry_new ();
	gtk_widget_set_sensitive (entry, FALSE);
	add_sources (E_CONTACT_ENTRY (entry));
	setup_source_changes (E_CONTACT_ENTRY (entry));
	e_contact_entry_set_search_fields (E_CONTACT_ENTRY (entry), (const EContactField *)fields);
	e_contact_entry_set_display_func (E_CONTACT_ENTRY (entry), test_display_func, NULL, NULL);
	g_signal_connect (G_OBJECT (entry), "contact_selected",
			G_CALLBACK (contact_selected_cb), NULL);
	g_signal_connect (G_OBJECT (window), "delete-event",
			G_CALLBACK (window_closed), NULL);
	g_signal_connect (G_OBJECT (entry), "changed",
			G_CALLBACK (text_changed), NULL);
	g_signal_connect (G_OBJECT (entry), "state-change",
			G_CALLBACK (state_change_cb), NULL);

	gtk_container_add (GTK_CONTAINER (gtk_dialog_get_content_area (GTK_DIALOG (window))), entry);

	gtk_widget_show_all (window);

	gtk_dialog_run (GTK_DIALOG (window));
	g_print ("Phone number selected: %s\n", phone_number);

	return 0;
}

