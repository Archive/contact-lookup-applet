/* 
 * Copyright (C) 2004 Ross Burton <ross@burtonini.com>
 *
 * Contact Lookup Applet
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Ross Burton <ross@burtonini.com>
 */

#include <libebook/e-contact.h>

#define LEN_CONTACTINFO_ADDRESS_NAMES 3

typedef struct _EContactInfo {
  EContactField field;
  const char *image;
} EContactInfo;

EContactInfo contactinfo_address[LEN_CONTACTINFO_ADDRESS_NAMES+1];
const char *contactinfo_address_names[LEN_CONTACTINFO_ADDRESS_NAMES];

EContactInfo contactinfo_im[37];

EContactInfo contactinfo_email[5];
