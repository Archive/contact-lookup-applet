/* 
 * Copyright (C) 2003 Ross Burton <ross@burtonini.com>
 *
 * Contact Lookup Applet
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Authors: Ross Burton <ross@burtonini.com>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif /* HAVE_CONFIG_H */

#include "contact-fields.h"

#include <string.h>
#include <glib/gi18n.h>
#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>
#include <libebook/e-contact.h>
#include <libgnome/gnome-url.h>

#include "glade-utils.h"

typedef struct _addr_cb_data {
  GtkLabel *label;
  EContact *contact;
} addr_cb_data;

static void
homepage_clicked_cb (GtkButton *button, const char* homepage)
{
  GError *error = NULL;
  char *url;
  g_return_if_fail (homepage != NULL);
  /* Handle URLs without a protocol by appending http:// */
  if (!strstr (homepage, "://")) {
    url = g_strdup_printf ("http://%s", homepage);
  } else {
    url = g_strdup (homepage);
  }
  if (!gnome_url_show (url, &error)) {
    GtkWidget *dialog;
    dialog = gtk_message_dialog_new_with_markup(NULL, 0, GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE,
                                                _("<span weight=\"bold\" size=\"large\">Cannot show URL %s:</span>\n\n%s"),
                                                url, error->message);
    g_error_free (error);
    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(dialog);
  }
  g_free (url);
}

static void
address_selection_changed_cb (GtkComboBox *combo, addr_cb_data *addr_cb_ptr)
{
  GtkLabel *address_label;
  EContact *contact;
  GtkTreeIter iter;
  const char *new_address = NULL;
  guint16 i;

  address_label = addr_cb_ptr->label;
  g_return_if_fail (address_label != NULL);
  contact = addr_cb_ptr->contact;

  if (gtk_combo_box_get_active_iter (GTK_COMBO_BOX (combo), &iter)) {
    GtkTreeModel *model;
    char *combo_sel;

    model = gtk_combo_box_get_model (GTK_COMBO_BOX (combo));
    gtk_tree_model_get (model, &iter, 1, &combo_sel, -1);

    for (i = 0; i < LEN_CONTACTINFO_ADDRESS_NAMES; i++)
      if (strcmp (contactinfo_address_names[i], combo_sel) == 0) {
        new_address = e_contact_get_const (contact,
                                            contactinfo_address[i].field);
        break;
      }

    if (new_address) {
      gtk_label_set_text (GTK_LABEL (address_label), new_address);
    }
    g_free (combo_sel);
  } else {
    for (i = 0; i < LEN_CONTACTINFO_ADDRESS_NAMES; i++) {
      new_address = e_contact_get_const (contact, contactinfo_address[i].field);
      if (new_address) {
        gtk_label_set_text (GTK_LABEL (address_label), new_address);
        break;
      }
    }
  }
}

static void
send_email_clicked_cb (GtkButton *button, GtkWidget *combo)
{
  GtkTreeIter iter;
  GError *error = NULL;
  g_return_if_fail (combo != NULL);
  
  if (gtk_combo_box_get_active_iter (GTK_COMBO_BOX (combo), &iter)) {
    GtkTreeModel *model;
    char *email, *url;

    model = gtk_combo_box_get_model (GTK_COMBO_BOX (combo));
    gtk_tree_model_get (model, &iter, 1, &email, -1);

    /* TODO: URL-encode email */
    url = g_strdup_printf("mailto:%s", email);
    if (!gnome_url_show (url, &error)) {
      GtkWidget *dialog;
      dialog = gtk_message_dialog_new_with_markup(NULL, 0, GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE,
                                                  _("<span weight=\"bold\" size=\"large\">Cannot send email to %s:</span>\n\n%s"),
                                                  email, error->message);
      g_error_free (error);
      gtk_dialog_run(GTK_DIALOG(dialog));
      gtk_widget_destroy(dialog);
    }
    g_free (url);
    g_free (email);
  }
}

static void
video_clicked_cb (GtkButton *button, const char* url)
{
  GError *error = NULL;
  g_return_if_fail (url != NULL);
  if (!gnome_url_show (url, &error)) {
    GtkWidget *dialog;
    dialog = gtk_message_dialog_new_with_markup(NULL, 0, GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE,
                                                _("<span weight=\"bold\" size=\"large\">Cannot start conference with %s:</span>\n\n%s"),
                                                url, error->message);
    g_error_free (error);
    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(dialog);
  }
}

/**
 * Callback when a key is pressed. Used to catch the escape key.
 * TODO: press? release?
 */
static gboolean
keypress_event_cb (GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{
  if (event->keyval == GDK_Escape) {
    gtk_widget_destroy (widget);
    return TRUE;
  }
  return FALSE;
}

static void
destroy_cb (GtkObject *object, EContact *contact)
{
  g_return_if_fail (E_IS_CONTACT (contact));
  g_object_unref (contact);
}


static int
populate_from_fields (GtkListStore *store, EContactInfo fields[], EContact *contact, const char *identifier) {
  int i, ret;

  ret = -1;

  for(i = 0; fields[i].field != -1; i++) {
    GtkTreeIter iter;
    GdkPixbuf *pixbuf;
    const char *data;

    data = e_contact_get_const (contact, fields[i].field);
    if (data && *data != '\0') {
      if (ret < 0 && identifier != NULL && strcmp (identifier, data) == 0)
        ret = i;

      if (fields[i].image) {
        pixbuf = get_icon (fields[i].image, 16);
      } else {
        pixbuf = NULL;
      }

      gtk_list_store_append (store, &iter);
      gtk_list_store_set (store, &iter,
                          0, pixbuf,
                          1, data,
                          -1);
    }
  }

  if (ret < 0)
    ret = 0;

  return ret;
}


static void create_combo(EContact *contact, const char *identifier, GtkWidget *combo, GtkWidget *box, EContactInfo fields[]) {
  GtkListStore *store;
  GtkCellRenderer *renderer;
  int selected;
  
  store = gtk_list_store_new (2, GDK_TYPE_PIXBUF, G_TYPE_STRING);
  renderer = gtk_cell_renderer_pixbuf_new ();
  gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (combo), renderer, FALSE);
  gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (combo), renderer, "pixbuf", 0, NULL);
  
  renderer = gtk_cell_renderer_text_new ();
  gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (combo), renderer, TRUE);
  gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (combo), renderer, "text", 1, NULL);
  
  selected = populate_from_fields (store, fields, contact, identifier);
  
  if (gtk_tree_model_iter_n_children (GTK_TREE_MODEL (store), NULL) > 0) {
    gtk_combo_box_set_model (GTK_COMBO_BOX (combo), GTK_TREE_MODEL (store));
    gtk_combo_box_set_active (GTK_COMBO_BOX (combo), selected);
  } else {
    gtk_widget_hide (box);
  }
}

static void
populate_from_fields_name (GtkListStore *store, EContactInfo fields[], const char *names[], EContact *contact) {
  int i;

  for(i = 0; fields[i].field != -1; i++) {
    GtkTreeIter iter;
    GdkPixbuf *pixbuf;
    const char *data;


    data = e_contact_get_const (contact, fields[i].field);

    if (data && *data != '\0') {
      if (fields[i].image) {
        pixbuf = get_icon (fields[i].image, 16);
      } else {
        pixbuf = NULL;
      }

      gtk_list_store_append (store, &iter);
      gtk_list_store_set (store, &iter,
                          0, pixbuf,
                          1, _(names[i]),
                          -1);
    }
  }
}

static void create_combo_name(EContact *contact, GtkWidget *combo, GtkWidget *box, EContactInfo fields[], const char *names[]) {
  GtkListStore *store;
  GtkCellRenderer *renderer;
  guint16 num_combo_entries = 0;
  
  store = gtk_list_store_new (2, GDK_TYPE_PIXBUF, G_TYPE_STRING);
  renderer = gtk_cell_renderer_pixbuf_new ();
  gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (combo), renderer, FALSE);
  gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (combo), renderer, "pixbuf", 0, NULL);
  
  renderer = gtk_cell_renderer_text_new ();
  gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (combo), renderer, TRUE);
  gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (combo), renderer, "text", 1, NULL);
  
  populate_from_fields_name (store, fields, names, contact);
  
  if ((num_combo_entries = 
        gtk_tree_model_iter_n_children (GTK_TREE_MODEL (store), NULL)) > 1) {
    gtk_combo_box_set_model (GTK_COMBO_BOX (combo), GTK_TREE_MODEL (store));
    gtk_combo_box_set_active (GTK_COMBO_BOX (combo), 0);
  } else {
    if (num_combo_entries == 1) {
      gtk_widget_hide (combo);
    } else {
      gtk_widget_hide (box);
    }
  }
}


/**
 * Create a new dialog showing the details of a specified contact. This dialog
 * will destroy itself as required.
 */
GtkWidget *contact_dialog_new (EContact *contact, const char *identifier)
{
  GtkWidget *dialog;
  GtkWidget *photo_image, *photo_frame;
  GtkWidget *address_box, *address_label, *address_combo;
  GtkWidget *name_label, *phone_label;
  GtkWidget *email_box, *email_image, *email_combo, *email_button;
  GtkWidget *im_box, *im_image, *im_combo, *im_button;
  GtkWidget *video_box, *video_image, *video_label, *video_button;
  GtkWidget *homepage_box, *homepage_label, *homepage_button;

  g_return_val_if_fail (E_IS_CONTACT (contact), NULL);
  g_object_ref (contact);

#if 0  
  /* Debugging */
  e_vcard_dump_structure ((EVCard*)contact);
#endif

  utils_glade_get_file_simple (PKGDATADIR"/contact-lookup-applet.ui",
                               "dialog", &dialog,
                               "photo_image", &photo_image,
                               "photo_frame", &photo_frame,
                               "name_label", &name_label,
                               "address_box", &address_box,
                               "address_label", &address_label,
                               "address_combo", &address_combo,
                               "phone_label", &phone_label,
                               "email_box", &email_box,
                               "email_image", &email_image,
                               "email_combo", &email_combo,
                               "email_button", &email_button,
                               "im_box", &im_box,
                               "im_image", &im_image,
                               "im_combo", &im_combo,
                               "im_button", &im_button,
                               "video_box", &video_box,
                               "video_image", &video_image,
                               "video_label", &video_label,
                               "video_button", &video_button,
                               "homepage_box", &homepage_box,
                               "homepage_label", &homepage_label,
                               "homepage_button", &homepage_button,
                               NULL);

  /* TODO: calculate size */
  theme_image (GTK_IMAGE (email_image), "stock_mail-open", 32);
  theme_image (GTK_IMAGE (im_image), "stock_person", 32);
  theme_image (GTK_IMAGE (video_image), "stock_landline-phone", 32);

  /* TODO: add size groups in glade... */
  {
    GtkSizeGroup *size;
    size = gtk_size_group_new (GTK_SIZE_GROUP_HORIZONTAL);
    gtk_size_group_add_widget (size, email_button);
    gtk_size_group_add_widget (size, im_button);
    gtk_size_group_add_widget (size, video_button);
    g_object_unref (size);
  }

  {
    EContactPhoto *photo;
    PangoLayout *layout;
    gint max_height;

    gchar *name = g_markup_printf_escaped ("<span weight=\"bold\" size=\"large\">%s</span>", (char *)e_contact_get_const (contact, E_CONTACT_FULL_NAME));
    gtk_label_set_markup (GTK_LABEL (name_label), name);
    g_free (name);

    layout = gtk_label_get_layout (GTK_LABEL (name_label));
    pango_layout_get_pixel_size (layout, NULL, &max_height);

    max_height = max_height * 4;
    
    photo = e_contact_get (contact, E_CONTACT_PHOTO);
#ifndef HAVE_ECONTACTPHOTOTYPE
    if (photo) {
#else
    if (photo && photo->type == E_CONTACT_PHOTO_TYPE_INLINED) {
#endif
      GdkPixbufLoader *loader;
      GdkPixbuf *pixbuf;

      loader = gdk_pixbuf_loader_new ();

#ifndef HAVE_ECONTACTPHOTOTYPE
      if (!gdk_pixbuf_loader_write (loader, photo->data, photo->length, NULL)) {
#else
      if (!gdk_pixbuf_loader_write (loader, (guchar *)photo->data.inlined.data, photo->data.inlined.length, NULL)) {
#endif
        gtk_widget_hide (photo_frame);
      } else if( (pixbuf = gdk_pixbuf_loader_get_pixbuf (loader)) != NULL ) {
        GdkPixbuf *tmp;
        gint width = gdk_pixbuf_get_width (pixbuf);
        gint height = gdk_pixbuf_get_height (pixbuf);
        double scale = 1.0;

        /* Scale down some potentially large images */
        if (height > width) {
          scale = max_height / (double) height;
        } else {
          scale = max_height / (double) width;
        }

        if (scale < 1.0) {
         tmp = gdk_pixbuf_scale_simple (pixbuf, width * scale, height * scale, GDK_INTERP_BILINEAR);
         g_object_unref (pixbuf);
         pixbuf = tmp;
        }

        gtk_image_set_from_pixbuf (GTK_IMAGE (photo_image), pixbuf);
        gtk_window_set_icon (GTK_WINDOW (dialog), pixbuf);
        g_object_unref (pixbuf);
      }
    } else {
      gtk_widget_hide (photo_frame);
    }
    if (photo)
      e_contact_photo_free (photo);
  }
    
  {
    /* Phone */
    const char* phone = NULL;
    GString *s;
    s = g_string_new (NULL);

    phone = e_contact_get_const (contact, E_CONTACT_PHONE_HOME);
    if (phone) {
      g_string_append (s, phone);
      g_string_append (s, _(" (home)\n"));
    }

    phone = e_contact_get_const (contact, E_CONTACT_PHONE_HOME_2);
    if (phone) {
      g_string_append (s, phone);
      g_string_append (s, _(" (home)\n"));
    }
    
    phone = e_contact_get_const (contact, E_CONTACT_PHONE_BUSINESS);
    if (phone) {
      g_string_append (s, phone);
      g_string_append (s, _(" (work)\n"));
    }

    phone = e_contact_get_const (contact, E_CONTACT_PHONE_BUSINESS_2);
    if (phone) {
      g_string_append (s, phone);
      g_string_append (s, _(" (work)\n"));
    }
    
    phone = e_contact_get_const (contact, E_CONTACT_PHONE_MOBILE);
    if (phone) {
      g_string_append (s, phone);
      g_string_append (s, _(" (mobile)\n"));
    }
    
    if (s->len > 0) {
      gtk_label_set_text (GTK_LABEL (phone_label), s->str);
    } else {
      gtk_widget_hide (phone_label);
    }
    g_string_free (s, TRUE);
  }
    
  {
    const char* address;

    /* Mailing Address */
    addr_cb_data *addr_cb = g_malloc(sizeof(addr_cb_data));
    addr_cb->label = GTK_LABEL (address_label);
    addr_cb->contact = contact;
    create_combo_name (contact, address_combo, address_box, contactinfo_address,
                        contactinfo_address_names);
    /* Manually call the callback the first time, to make normal flow less
     * special-cased */
    address_selection_changed_cb(GTK_COMBO_BOX (address_combo), addr_cb);
    g_signal_connect_data (address_combo, "changed", (GCallback)address_selection_changed_cb, addr_cb, (GClosureNotify) g_free, 0);
   
    /* Home Page */
    address = e_contact_get_const (contact, E_CONTACT_HOMEPAGE_URL);
    if (address && *address != '\0') {
      gtk_label_set_text (GTK_LABEL (homepage_label), address);
      g_signal_connect (homepage_button, "clicked", (GCallback)homepage_clicked_cb, (char*)address);
    } else {
      gtk_widget_hide (homepage_box);
    }

    /* Video conferencing */
    address = e_contact_get_const (contact, E_CONTACT_VIDEO_URL);
    if (address && *address != '\0') {
      gtk_label_set_text (GTK_LABEL (video_label), address);
      g_signal_connect (video_button, "clicked", (GCallback)video_clicked_cb, (char*)address);
    } else {
      gtk_widget_hide (video_box);
    }
  }

  /* Instant Messaging */
  create_combo (contact, NULL, im_combo, im_box, contactinfo_im);

  /* Email */
  create_combo (contact, identifier, email_combo, email_box, contactinfo_email);
  g_signal_connect (email_button, "clicked", (GCallback)send_email_clicked_cb, email_combo);

  g_signal_connect (dialog, "key-press-event", (GCallback)keypress_event_cb, NULL);
  g_signal_connect (dialog, "destroy", (GCallback)destroy_cb, contact);
  
  return dialog;
}
