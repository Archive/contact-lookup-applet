
#include <gtk/gtk.h>
#include <string.h>
#include <libedataserver/e-source-list.h>
#include "e-contact-entry.h"

#define GCONF_COMPLETION "/apps/evolution/addressbook"
#define GCONF_COMPLETION_SOURCES GCONF_COMPLETION "/sources"

#define CONTACT_FORMAT "%s <%s>"

static char *email = NULL;
static char *name = NULL;
static GtkWidget *activate;

static void
add_sources (EContactEntry *entry)
{
	ESourceList *source_list;

	source_list =
		e_source_list_new_for_gconf_default (GCONF_COMPLETION_SOURCES);
	e_contact_entry_set_source_list (E_CONTACT_ENTRY (entry),
			source_list);
	g_object_unref (source_list);
}

static void
sources_changed_cb (GConfClient *client, guint cnxn_id,
		GConfEntry *entry, EContactEntry *entry_widget)
{
	add_sources (entry_widget);
}

static void
setup_source_changes (EContactEntry *entry)
{
	GConfClient *gc;

	gc = gconf_client_get_default ();
	gconf_client_add_dir (gc, GCONF_COMPLETION,
			GCONF_CLIENT_PRELOAD_ONELEVEL, NULL);
	gconf_client_notify_add (gc, GCONF_COMPLETION,
			(GConfClientNotifyFunc) sources_changed_cb,
			entry, NULL, NULL);
}

static void
contact_selected_cb (GtkWidget *entry, EContact *contact, const char *identifier)
{
	char *text;

	g_free (email);
	email = NULL;

	if (identifier != NULL)
		email = g_strdup (identifier);
	else
		email = e_contact_get (contact, E_CONTACT_EMAIL_1);

	g_free (name);
	name = NULL;

	name = e_contact_get (contact, E_CONTACT_FULL_NAME);
	if (name == NULL) {
		name = e_contact_get (contact, E_CONTACT_NICKNAME);
		if (name == NULL)
			name = e_contact_get (contact, E_CONTACT_ORG);
	}
	if (name != NULL) {
		text = g_strdup_printf (CONTACT_FORMAT, (char*) name, email);
		gtk_entry_set_text (GTK_ENTRY (entry), text);
		g_free (text);
	} else
		gtk_entry_set_text (GTK_ENTRY (entry), email);
}

static gboolean
window_closed (GtkWidget *widget, GdkEvent *event, gpointer data)
{
	g_print ("E-mail selected: %s <%s>\n", name, email);
	exit (0);
	return FALSE;
}

static void
state_change_cb (GtkWidget *entry, gboolean state, gpointer data)
{
	g_print ("State of the entry changed: %s\n", state ? "Enabled" : "Disabled");
	gtk_widget_set_sensitive (entry, state);
}

int main (int argc, char **argv)
{
	GtkWidget *window, *entry;

	gtk_init (&argc, &argv);

	window = gtk_dialog_new ();
	activate = gtk_dialog_add_button (GTK_DIALOG (window), GTK_STOCK_OK, GTK_RESPONSE_OK);
	entry = e_contact_entry_new ();
	gtk_widget_set_sensitive (entry, FALSE);
	add_sources (E_CONTACT_ENTRY (entry));
	setup_source_changes (E_CONTACT_ENTRY (entry));
	g_signal_connect (G_OBJECT (entry), "contact_selected",
			G_CALLBACK (contact_selected_cb), NULL);
	g_signal_connect (G_OBJECT (window), "delete-event",
			G_CALLBACK (window_closed), NULL);
	g_signal_connect (G_OBJECT (entry), "state-change",
			G_CALLBACK (state_change_cb), NULL);

	gtk_container_add (GTK_CONTAINER (gtk_dialog_get_content_area (GTK_DIALOG (window))), entry);

	gtk_widget_show_all (window);

	gtk_dialog_run (GTK_DIALOG (window));
	g_print ("E-mail selected: %s <%s>\n", name, email);

	return 0;
}

